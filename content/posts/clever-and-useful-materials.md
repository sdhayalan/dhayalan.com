---
date: 2018-11-09
title: Clever and Useful Finds
draft: false
---

The following are a non-comprehensive list of articles and links I have come across that are note-worthy

* NES.css: 8-bit style CSS framework - https://bcrikko.github.io/NES.css/
* 12 Factor CLI Apps - https://medium.com/@jdxcode/12-factor-cli-apps-dd3c227a0e46
* Modern Backend Developer - https://medium.com/tech-tajawal/modern-backend-developer-in-2018-6b3f7b5f8b9   
* [The Importance of Keeping A Notebook] (https://blogs.psychcentral.com/everyday-creativity/2015/05/the-importance-of-keeping-a-notebook/?utm_source=pocket&utm_medium=email&utm_campaign=pockethits)
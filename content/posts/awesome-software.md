---
date: 2018-11-10
title: Awesome Software
draft: true
---

The following is a list of software I have found before but I could never rememeber the name when I am trying to brag about them to someone. So, I am putting it back where I found them.


## Windows Tools
* https://the-eye.eu/public/MSDN/
* https://github.com/Nummer/Destroy-Windows-10-Spying


## Interesting
* http://mute.fm/ 
 
## Web Services  
* http://www.10minutemail.com/
* https://privacy.com/ - create virtual credit cards
* http://gen.lib.rus.ec/ - ebooks
* http://gen.lib.rus.ec/ - ebooks
* https://weboas.is/ - new tab
* http://www.myfridgefood.com/ - food
* https://opencourser.com/ - search online courses
* https://www.pcpartpicker.com/
* http://www.gnod.com/ - get movie suggestions based on your favourites
* https://ninite.com/
* http://pcottle.github.io/MSOutlookit/ - make reddit look like outlook
* https://www.productchart.com - research consumer electronics
* http://radio.garden/ - radio
* http://usefulinterweb.com/ - useful websites
* http://www.wolframalpha.com/
* https://tcrf.net/The_Cutting_Room_Floor - easter eggs
* TheNounProject.com - find icons
* https://www.flaticon.com/ - find icons
* http://magzdb.org/j/1828 - magazines
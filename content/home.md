+++
title = "Home"
+++

Hi there,
I am Siva. 

I am a Ryerson University graduate, currently working as a Full Stack Web Developer in Toronto. I'm also a coffee-lover, environmentalist, minimalist, novice photographer and a self-proclaimed car racer, among other things.

Check out [my projects](https://dhayalan.com/projects/) or [my photos](https://www.instagram.com/sdhayalan/).
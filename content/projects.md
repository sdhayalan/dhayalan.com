+++
date = "2015-08-22"
title = "projects"
+++

These projects are done in my free time and most of them are as old as 15 years. Work projects are not listed here, but they showcase my skills better than any of these projects do. Contact me if you want to know about my work projects. Thank you.

#### [Simple Sketch](https://play.google.com/store/apps/details?id=com.sivasuthan.simplesketch) - Android Application
* I got tired of feature-rich and slow note-taking apps that require whole bunch of permissions to run. So, I made my own simple app to draw and save hand-drawn notes. The only permission it requires is the access to Storage to save the created note. Not even a permission to connect to Internet is required, and it loads super fast.

#### [Chrome Sound Effects](https://chrome.google.com/webstore/detail/chrome-sound-effects/oekengelpdnkfopdkkphkmaacfanbnla?utm_source=chrome-ntp-icon) - Chrome extension
* Add sounds to events triggered by user in Chrome, like switching tabs, etc. They can be manually disabled.

#### [Change all UI fonts](https://chrome.google.com/webstore/detail/change-all-ui-fonts/loiejdbcheeiipmakhghinclmpafiiel?utm_source=chrome-ntp-icon) - Chrome extension
* Changes all the font-faces on all the web sites to a user given local font. Enter your desired font in the options by clicking the extension icon.

#### [Weather app](https://progressive-web-apps-736ed.firebaseapp.com/) - Progressive Web App
* Made this on a Google Developer workshop called Progressive Web Apps. I used Service workers to do the updates and show cache data while offline.

#### [Google Search Widget](/projects/win/google-search.exe) - Native Windows Application
* Type text and hit enter to search that text.

#### [Oscar Winner Finder](#) - Simple PHP App (too simple to link)
* Used PHP and MySQL to implement a simple search function to retrieve the title, year and Rotten Tomatoes score of the Academic Award-winning feature films.

#### [Thirukural (திருக்குறள்)](https://github.com/dsivasuthan/thirukurals)
* Thiruvalluvar wrote this classic Tamil sangam literature, consisting of 1330 couplets or Kurals, sometime between the third and first centuries BCE. I came across this compilation some time ago and made a copy/backup for myself. Thanks to all those who help compile this timeless piece of writing.

### Old Stuff:

#### [my old homepage](/projects/old_stuff/HTML.htm)
* This links to an old "website" I had built about 15 years ago

### School Projects:

#### [lifelens](http://sivasuthan.com/lifelens/)
* A simple photo curation web service. What is special about this service is that it’s designed to present user photos in a Timeline view. The algorithm extracts EXIF data from the uploaded photos and creates the Timeline by itself without user interaction. Just view one example.

#### [chatter](http://www2.scs.ryerson.ca/~sdhayala/cgi-bin/project/)
* A Twitter-like public chat engine, very simple implementation using Perl and php.

#### Stuff I modified from other sites:

* [Onscreen Keyboards for Tamil and Sinhala](/projects/kbconverters/kbconverters.htm)



___

** UNDER CONSTRUCTION – more on the way
